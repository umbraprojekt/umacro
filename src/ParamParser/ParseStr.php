<?php
namespace UmbraProjekt\uMacro\ParamParser;

use UmbraProjekt\uMacro\ParamParserInterface;

/**
 * Params parser that uses plain parse_str() to parse the parametres.
 */
class ParseStr implements ParamParserInterface
{
	/**
	 * @inheritdoc
	 */
	public function parse($paramsString)
	{
		parse_str($paramsString, $params);

		return $params;
	}
}
